import socket
import json
import os
import time
import hashlib
from _thread import *
import psycopg2
import datetime as dt
from dateutil.parser import parse
import os

from cryptography.hazmat.primitives import serialization

from Encrypt import Encryption_module
from auth_err_codes import *

DELIMITER = b'\x15'.decode('utf-8')

# Socket info
MAX_BUFFER = 4096
MAX_CLIENTS = 32
HOST = ''
PORT = 8069
TIMEOUT_INTERVAL = float(os.environ['TIMEOUT_INTERVAL'])
DB_HOSTNAME = os.environ['DB_HOSTNAME']
DB_PORT = os.environ['DB_PORT']
DB_NAME = os.environ['DB_NAME']
DB_USERNAME = os.environ['DB_USERNAME']
DB_PASSWORD = os.environ['DB_PASSWORD']

users_check_user = """SELECT * from Users where Username = %s"""
users_insert = """ INSERT INTO Users (Username, Password_hash) VALUES (%s,%s)"""
user_update = """UPDATE Users set Token = %s, Sess_updated = %s where Username = %s"""
update_session = """UPDATE Users set Sess_updated = %s where Username = %s"""

class Authenticator:

    def __init__(self, db_cursor, db_connection):

        self.db_cursor = db_cursor
        self.db_connection = db_connection

        self.socket = socket.socket()
        self.encryption_module = Encryption_module()

        self.go_on = True

        try:
            self.socket.bind((HOST, PORT))
        except socket.error as e:
            print(str(e))
    
        self.socket.listen(MAX_CLIENTS)

    def start_loop(self):
        print("Starting loop")
        while self.go_on:
            try:
                client, address = self.socket.accept()
                start_new_thread(self.conn_thread_behaviour, (client, address))
            except Exception as e:
                print('Closing server due to unexpected error\n{}'.format(str(e)))
                self.socket.close()

    def conn_thread_behaviour(self, conn, address):
        # Define the behavior of a threat
        data = conn.recv(MAX_BUFFER)
        status, response = self.parse_command(data)

        conn.send(bytes((str(status)+' ').encode('utf-8'))+response)
        conn.close()

    def send_public_key(self):
        return WRONG_KEY, self.encryption_module.get_public_key()

    def reconnect(self):
        try:
            self.db_connection = psycopg2.connect(host = DB_HOSTNAME, port = DB_PORT, dbname = DB_NAME, user = DB_USERNAME, password = DB_PASSWORD)
            self.db_cursor = self.db_connection.cursor()
        except Exception as e:
            print(e)
            exit(0)
    
    def create_account(self, user_public_key, username, password_hash):
        try:
            self.db_cursor.execute(users_check_user, (username,))
            user_record = self.db_cursor.fetchone()
            if user_record is not None:
                return USERNAME_TAKEN, self.encryption_module.encrypt(user_public_key, 'Username already taken')
        except (Exception, psycopg2.Error) as error:
            print("Interface error, trying to reconnect")
            time.sleep(1)
            self.reconnect()
            self.db_connection.rollback()
            print("Create account get user conflict", error)
            return DB_ERROR, self.encryption_module.encrypt(user_public_key, 'Create account get user conflict')

        try:
            self.db_cursor.execute(users_insert, (username, password_hash,))
            self.db_connection.commit()
        except (Exception, psycopg2.Error) as error:
            print("Interface error, trying to reconnect")
            time.sleep(1)
            self.reconnect()
            self.db_connection.rollback()
            print("Create account insert user conflict", error)
            return DB_ERROR, self.encryption_module.encrypt(user_public_key, 'Create account insert user conflict')

        return SUCCESS, self.encryption_module.encrypt(user_public_key, 'Account created')

    @staticmethod
    def get_token(username, password):
        data_base = str(username) + str(password)
        data_base += str(time.time())
        token = hashlib.sha256(data_base.encode()).hexdigest()
        return token.encode()

    def log_in(self, user_public_key, username, password_hash):
        print("!!!we've logged in!!!")
        try:
            self.db_cursor.execute(users_check_user, (username,))
            user_record = self.db_cursor.fetchone()
            if user_record is None:
                return USERNAME_404, self.encryption_module.encrypt(user_public_key, 'Username does not exist')
        except (Exception, psycopg2.Error) as error:
            print(type(error))
            print("Interface error, trying to reconnect")
            time.sleep(1)
            self.reconnect()
            self.db_connection.rollback()
            print('Log in get user conflict', error)
            return DB_ERROR, self.encryption_module.encrypt(user_public_key, 'Log in get user conflict')

        pass_hash = user_record[2]
        if pass_hash != password_hash:
            return PASS_INCORRECT, self.encryption_module.encrypt(user_public_key, 'Incorrect password')

        token = self.get_token(username, pass_hash)

        try:
            self.db_cursor.execute(user_update, (token.decode(), str(dt.datetime.now()), username,) )
            self.db_connection.commit()
        except (Exception, psycopg2.Error) as error:
            print(type(error))
            print("Interface error, trying to reconnect")
            time.sleep(1)
            self.reconnect()
            self.db_connection.rollback()
            print("conflict la update log in", error)
            return DB_ERROR, self.encryption_module.encrypt(user_public_key, "conflict la update log in")

        return SUCCESS, self.encryption_module.encrypt( user_public_key, token, is_bytes=True)
    
    def log_out(self, user_public_key, username, token):

        ret_code, msg = self.check_token(user_public_key, username, token)

        if ret_code is not SUCCESS:
            return ret_code, msg

        try:
            self.db_cursor.execute(user_update, (None, str(dt.datetime.now()), username,) )
            self.db_connection.commit()
        except (Exception, psycopg2.Error) as error:
            print(type(error))
            print("Interface error, trying to reconnect")
            time.sleep(1)
            self.reconnect()
            self.db_connection.rollback()
            print("conflict la log out", error)
            return DB_ERROR, self.encryption_module.encrypt(user_public_key, "conflict la update log out")
        
        return SUCCESS, self.encryption_module.encrypt(user_public_key, 'Successfully logged out')

    def check_token(self, user_public_key, username, token):

        try:
            self.db_cursor.execute(users_check_user, (username,))
            user_record = self.db_cursor.fetchone()
            if user_record is None:
                return USERNAME_404, self.encryption_module.encrypt(user_public_key, 'Username does not exist')
        except (Exception, psycopg2.Error) as error:
            print(type(error))
            print("Interface error, trying to reconnect")
            time.sleep(1)
            self.reconnect()
            self.db_connection.rollback()
            print('Log in get user conflict', error)
            return DB_ERROR, self.encryption_module.encrypt(user_public_key, 'Log in get user conflict')

        if user_record[3] != token:
            return TOKEN_INCORRECT, self.encryption_module.encrypt(user_public_key, 'Incorrect token')

        if (dt.datetime.now() - user_record[4]).total_seconds() > TIMEOUT_INTERVAL:
            return SESSION_EXPIRED, self.encryption_module.encrypt(user_public_key, 'Session expired')

        try:
            self.db_cursor.execute(update_session, (str(dt.datetime.now()), username,) )
            self.db_connection.commit()
        except (Exception, psycopg2.Error) as error:
            print(type(error))
            print("Interface error, trying to reconnect")
            time.sleep(1)
            self.reconnect()
            self.db_connection.rollback()
            print("conflict la update timestamp check in", error)
            return DB_ERROR, self.encryption_module.encrypt(user_public_key, "Conflict la update timestamp in check")
        
        return SUCCESS, self.encryption_module.encrypt(user_public_key, 'Good token')

    def parse_command(self, data):
        try:
            data = self.encryption_module.decrypt(data)
        except ValueError as e:
            print(e)
            return self.send_public_key()
        
        split = data.split(b' ',1)
        command = split[0].decode()
        print('command: ',command)
        split = split[1].split(b'-----END PUBLIC KEY-----')
        serialized_key = split[0]+b'-----END PUBLIC KEY-----'
        args = split[1].split(b' ')[1:]
        args = [a.decode() for a in args]
        print('args: ',args)
        user_public_key = serialization.load_pem_public_key(serialized_key)
    
        try:
            if command == 'CREATE_ACCOUNT':
                return self.create_account(user_public_key, args[0], args[1])    
            
            if command == 'LOG_IN':
                return self.log_in(user_public_key, args[0], args[1])
            
            if command == 'LOG_OUT':
                return self.log_out(user_public_key, args[0], args[1])

            if command == 'CHECK':
                return self.check_token(user_public_key, args[0], args[1])
            
            # if it's an unknown message we have three cases:
            # - it's a hello
            # - it's a valid message encoded with a public key no longer active
            # - it's garbage
            # In all three cases we respond by sending the current public key
            return self.send_public_key()

        except IndexError:
            return FAIL, 'Not enough arguments'

if __name__ == "__main__":
    try:
        db_connection = psycopg2.connect(host = DB_HOSTNAME, port = DB_PORT, dbname = DB_NAME, user = DB_USERNAME, password = DB_PASSWORD)
        db_cursor = db_connection.cursor()
    except Exception as e:
        print(e)
        exit(0)

    auth = Authenticator(db_cursor, db_connection)
    auth.start_loop()