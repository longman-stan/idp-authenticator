FROM python:3.8.2
COPY requirements.txt /tmp
RUN pip3 install -U setuptools
RUN pip3 install -r /tmp/requirements.txt
COPY Auth_server/ /app
WORKDIR /app

ENV DB_HOSTNAME=Auth_db
ENV DB_PORT=5432
ENV DB_NAME=Auth_users
ENV DB_USERNAME=Authenticator
ENV DB_PASSWORD=1234
ENV TIMEOUT_INTERVAL=3600.0

EXPOSE 8086

CMD ["python", "-u", "auth.py"]
