# Authenticator microservice

This is an authentication microservice written in Python with Docker usage in mind.

It's a small server that handles authoristation securely. For an example of usage see stub_client.

It takes as input messages of the form "command public_key par1 par2". They should be delivered encoded with the authentication server's public key for safety reasons.
The server expects messages encoded in this way. If they're not encoded with its public key, it will respond with its public key, so that the sender can send a valid
message. The server responses are in the form of a pair, ret_code and message. The return code is in plain text, but the message is encrypted with the public key the
user has provided. An exception are the invalid messages, in which case the message is also in plain text. In case of error, the ret_code is the error code and the
message is populated with a short description of the problem.

The accepted commands are the following:
- CREATE_ACCOUNT: given a username in par1 and a password hash in par2 stores in a local json file the pair, virtually creating the account and returning 1. If the 
ussername is already taken, it returns -1
- LOG_IN: given a username in par1 and a password hash in par2, checks if there's a correspnding entry in the users json file and if there is returns 1 and the 
authentication token. If the username cannot be found, -1 is returned. If the password is incorret, -2 is returned.
- CHECK: given a username in par1 and the token in par2, checks if the token for the given username is valid in the current session. Returns 1 if the token is correct, -1 if the username cannot be found 
and -2 if the token is not valid
- LOG_OUT: given a username and a token, logs out the user. Returns 1 on success, -1 if the user does not exist and -2 if the token is invalid

For a simple example on how to use the service, build the image with ```docker image build .```, start the service with ```docker container run -p 8069:8069 test_auth``` and run 
the  example script stub_client.py.

## Authenticator_bd update:

To make the service runnable in a distributed system of containers(ex. Swarm) I would have had to manually implement a consistency model, which I obviously didn't want to do, so
I've changed the idea of data storage and added a postgres database. Now each replica stores and queries the database when it's needed and it's the database job to keep everything
consistent. This has little to do with the user, apart from not being able to run the service as a standalone service: it needs another postgresql container. 

In the DB folder you can find the initialiization script for the database and a docker compose file for starting both this service and the required postgresql contianer. 

There has been a single major addition to the logic. When the user logs in successfully, a token is returned and a session timer starts. The token is valid for the duration of the
session timer. The duration is changeable by editing setting the environment variable TIMEOUT_INTERVAL(see below for description). The timer gets reseted every time the token
validity is checked, practically extending the validity of the session.

You can edit the following global variables. You can find the defaults in the dockerfile:

- TIMEOUT_INTERVAL, a float representing the period of time since user inactivity in which the token is regarded as valid
- DB_HOSTNAME, hostname of the database service
- DB_PORT, the port which will be used
- DB_NAME, the database name
- DB_USERNAME, the username for connecting to the db
- DB_PASSWORD, the password for connecting to the db

!!! In case the DB is not configured properly, the server will return the error code -100, which 
means he couldn't connect to the database.

To run the service all you have to do is download the folder DB, open a terminal in it and run ```docker compose up```.