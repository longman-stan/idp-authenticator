from enum import Enum
from os.path import split
import re
import hashlib
import socket
import re
from typing import get_type_hints

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import serialization

from Auth_server.Encrypt import Encryption_module

HOST = '127.0.0.1'
PORT = 8069

def send_message_to_server(message):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(message)
            data = s.recv(1024)
        return data
    except Exception as e:
        print(e)
        return 0, 'communication_error'

enc_mod = Encryption_module()

def split_message(message, chunk_size=128):

    reps = len(message) // chunk_size
    chunks = [message[i*chunk_size:i*chunk_size+chunk_size] for i in range(reps)]
    chunks.append( message[reps * chunk_size:])
    return chunks


def encode_server(public_key, message, is_bytes = False):
    if is_bytes is False:
        message = bytes(message, 'utf-8')
    chunks = split_message(message, chunk_size=128)
    rez = b''
    for chunk in chunks:
        encr_chunk = public_key.encrypt(
                chunk,
                padding.OAEP(                        
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None))
        rez += encr_chunk
    return rez

def process_response(data, get_key = False):
    print(data)
    if get_key:
        return data.decode().split(' ', 1)
    split = data.split(b' ',1)
    ret_code = int(split[0])
    msg = split[1]
    print(ret_code)
    return ret_code, msg

data =  send_message_to_server(b'HELLO')
ret_code, key = process_response(data, get_key=True)
server_public_key = serialization.load_pem_public_key(bytes(key.encode()))

print("got the key, sending again")

hasshed_password = hashlib.sha256('B'.encode()).hexdigest()

data = send_message_to_server(encode_server(server_public_key, 
                    b'CREATE_ACCOUNT '+ enc_mod.get_public_key() + b' A ' + hasshed_password.encode(), is_bytes = True))
ret_code, msg = process_response(data)
print(enc_mod.decrypt(msg))

data = send_message_to_server(encode_server(server_public_key, 
                    b'LOG_IN '+ enc_mod.get_public_key() + b' A ' + hasshed_password.encode(), is_bytes = True))
ret_code, msg = process_response(data)
token = enc_mod.decrypt(msg)
print(token)

data = send_message_to_server(encode_server(server_public_key, 
                    b'CHECK '+ enc_mod.get_public_key() + b' A ' + token,is_bytes = True))
ret_code, msg = process_response(data)
print(enc_mod.decrypt(msg))

data = send_message_to_server(encode_server(server_public_key, 
                    b'LOG_OUT '+ enc_mod.get_public_key() + b' A ' + token,is_bytes = True))
ret_code, msg = process_response(data)
print(enc_mod.decrypt(msg))





